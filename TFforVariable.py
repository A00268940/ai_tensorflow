'''
Created on 2019年8月2日

@author: lenovo
'''
import tensorflow as tf



a = tf.Variable(0,name="name")
mid = tf.add(a,1)
changeA = tf.assign(a, mid)
init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init)
    for _ in range(4):
        sess.run(mid)
        sess.run(changeA)
        print(sess.run(a))
        
        
        