'''
Created on 2019年8月3日

@author: lenovo
'''
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

#读取数据集
mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

#喂入组的大小，和组的数量
BATCH_SIZE = 100
n_BATCH = mnist.train.num_examples
image_size = 784
answer_size = 10

x = tf.placeholder(tf.float32, [None, image_size])
y = tf.placeholder(tf.float32, [None, answer_size])

#创建神经元
w = tf.Variable(tf.zeros([image_size, answer_size]))
b = tf.Variable(tf.zeros([answer_size]))
predict = tf.nn.softmax(tf.matmul(x, w)+b)

loss = tf.reduce_mean(tf.square(y-predict))

train_step = tf.train.GradientDescentOptimizer(0.2).minimize(loss)

init = tf.global_variables_initializer()

predict_summary = tf.equal(tf.argmax(y, 1), tf.argmax(predict, 1))

accuracy = tf.reduce_mean(tf.cast(predict_summary, tf.float32))

with tf.Session() as sess:
    sess.run(init)
    for each in range(21):
        for batch in range(n_BATCH):
            batch_x, batch_y = mnist.train.next_batch(BATCH_SIZE)
            sess.run(train_step, feed_dict={x: batch_x, y: batch_y})
            
        acc = sess.run(accuracy, feed_dict={x: mnist.test.images, y: mnist.test.labels})
        print("Iter :" + str(each)+"test accuracy:"+str(acc))



