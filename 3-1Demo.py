'''
Created on 2019年8月3日

@author: lenovo
'''
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

#数据集
x_data = np.linspace(-0.5,0.5,200)[:,np.newaxis]
noise = np.random.normal(0,0.02,x_data.shape)
y_data = np.square(x_data)+noise


x = tf.placeholder(tf.float32,[None,1])
y = tf.placeholder(tf.float32,[None,1])

#神经网络中间层
w1 = tf.Variable(tf.random_normal([1,10]))
biases_1 = tf.Variable(tf.zeros([1,10]))
w1out = tf.matmul(x, w1) + biases_1
l1 = tf.nn.tanh(w1out)

#神经网络输出层
w2 = tf.Variable(tf.random_normal([10,1]))
biases_2 = tf.Variable(tf.zeros([1,1]))#注意零的行列根据当前层的输出匹配
w2out = tf.matmul(l1, w2) + biases_2
predict = tf.nn.tanh(w2out)

loss = tf.reduce_mean(tf.square(predict-y))

train_step = tf.train.GradientDescentOptimizer(0.05).minimize(loss)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    for _ in range(5000):
        sess.run(train_step,feed_dict={x:x_data,y:y_data})
        
    #获取预测值
    predict_value = sess.run(predict,feed_dict={x:x_data})
    #画图
    plt.figure()
    plt.scatter(x_data,y_data)
    plt.plot(x_data,predict_value,'r-',lw=5)
    plt.show()
        
    
    
    

