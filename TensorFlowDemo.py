'''
Created on 2019年8月3日

@author: lenovo
'''
import tensorflow as tf
import numpy as np

x_data = np.random.rand(100)
y_data = x_data*0.2+0.3

b = tf.Variable(0.)
k = tf.Variable(0.)
y = x_data*k + b

loss = tf.reduce_mean(tf.square(y_data-y))
optimizer = tf.train.GradientDescentOptimizer(0.1)
train = optimizer.minimize(loss)

init = tf.global_variables_initializer()
with tf.Session() as sess:
    sess.run(init)
    for step in range(301):
        sess.run(train)
        if step%20 == 0:
            print(step,sess.run([k,b]),sess.run(loss))

