'''
Created on 2019年8月2日

@author: lenovo
'''
import tensorflow as tf


input1 = tf.placeholder(tf.float32)
input2 = tf.placeholder(tf.float32)
add = tf.add(input1,input2)


with tf.Session() as sess:
    print(sess.run(add,feed_dict={input1:[1.],input2:[2.]}))
    '''注意feed_dict不是可命名的变量'''