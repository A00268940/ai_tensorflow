

import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

#读取数据集
mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

batch_size = 100

n_batch = mnist.train.num_examples;


def weight_variable_init(shape):
    init = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(init)


def bias_init(shape):
    bias = tf.constant(0.1, shape=shape)
    return tf.Variable(bias)


#卷积层 padding如果是same，卷积之后的结果和输入结果一样
def conv2d(ax, sW):
    return tf.nn.conv2d(ax, sW, strides=[1, 1, 1, 1], padding='SAME')


#池化层
def max_pool_2x2(sx):
    return tf.nn.max_pool(sx, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')


x = tf.placeholder(tf.float32, [None, 784])
y = tf.placeholder(tf.float32, [None, 10])

x_image = tf.reshape(x, [-1, 28, 28, 1])


#初始化第一层卷积层参数
W_conv1 = weight_variable_init([5, 5, 1, 32])
b_conv1 = bias_init([32])
#relu激活函数
h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
h_pool1 = max_pool_2x2(h_conv1) #池化

#第二层卷积
W_conv2 = weight_variable_init([5, 5, 32, 64])
b_conv2 = bias_init([64])
#relu激活函数
h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
h_pool2 = max_pool_2x2(h_conv2) #池化

#第一次卷积得28*28，第一次池化得14*14 (28-2)/2 +1=14
#第二次卷积14*14，池化7*7


#全连接层
W_fc1 = weight_variable_init([7*7*64, 1024])
b_fc1 = bias_init([1024])

#扁平化处理
h_pool2_flat = tf.reshape(h_pool2, [-1, 7*7*64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat,W_fc1) + b_fc1)

keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
#第二全连接层
W_fc2 = weight_variable_init([1024, 10])
b_fc2 = bias_init([10])

prediction = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
#交叉熵代价函数
cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=prediction))
train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)

predict_summary = tf.equal(tf.argmax(y, 1), tf.argmax(prediction, 1))#argmax返回张量中最大的数的位置
accuracy = tf.reduce_mean(tf.cast(predict_summary, tf.float32))

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    writer = tf.summary.FileWriter('D:\IdeaWorkSpace\Tensorflow\logs', sess.graph)
    for each in range(11):
        for batch in range(n_batch):
            batch_x, batch_y = mnist.train.next_batch(batch_size)
            sess.run(train_step, feed_dict={x: batch_x, y: batch_y, keep_prob: 0.7})

        acc = sess.run(accuracy, feed_dict={x: mnist.test.images, y: mnist.test.labels, keep_prob: 0.7})
        print("Iter :" + str(each)+"test accuracy:"+str(acc))






