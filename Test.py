'''
Created on 2019年8月2日

@author: lenovo
'''
import tensorflow as tf




m1 = tf.constant([[3,3]])
m2 = tf.constant([[2],[3]])
m3 = tf.Variable([[1,2]])
product = tf.matmul(m1,m2)
sub = tf.subtract(m1,m3)
init = tf.global_variables_initializer()

print(product)
with tf.Session() as sess:
    sess.run(init)
    print(sess.run(product))
    print(sess.run(sub))
