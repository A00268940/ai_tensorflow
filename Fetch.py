'''
Created on 2019年8月2日

@author: lenovo
'''

import tensorflow as tf

m1 = tf.constant(1)
m2 = tf.constant(2)
m3 = tf.constant(3)
add = tf.add(m1,m2)
mul = tf.multiply(m3,add)

with tf.Session() as sess:
    print(sess.run([mul,add]))
    '''执行顺序步步向上'''