import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

#读取数据集
mnist = input_data.read_data_sets("MNIST_data", one_hot=True)

#喂入组的大小，和组的数量
BATCH_SIZE = 100
n_BATCH = mnist.train.num_examples
image_size = 784
answer_size = 10


x = tf.placeholder(tf.float32, [None, image_size])
y = tf.placeholder(tf.float32, [None, answer_size])
keep_prob = tf.placeholder(tf.float32)
lr = tf.Variable(0.05, dtype=float)

#创建神经元
w1 = tf.Variable(tf.truncated_normal([image_size, 50], stddev=0.1))
b1 = tf.Variable(tf.zeros([50])+0.1)
l1 = tf.nn.tanh(tf.matmul(x, w1)+b1)
l1_drop = tf.nn.dropout(l1, keep_prob)
w2 = tf.Variable(tf.truncated_normal([50, answer_size], stddev=0.1))
b2 = tf.Variable(tf.zeros([answer_size])+0.1)
predict = tf.nn.softmax(tf.matmul(l1_drop, w2)+b2)


#reduce_mean用来降维求平均值
loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=tf.matmul(l1_drop, w2)+b2))

train_step = tf.train.AdadeltaOptimizer(lr).minimize(loss)

init = tf.global_variables_initializer()

predict_summary = tf.equal(tf.argmax(y, 1), tf.argmax(predict, 1))

accuracy = tf.reduce_mean(tf.cast(predict_summary, tf.float32))

with tf.Session() as sess:
    sess.run(init)
    for each in range(11):
        tf.assign(lr, 0.05*(0.95 ** each))
        for batch in range(n_BATCH):
            batch_x, batch_y = mnist.train.next_batch(BATCH_SIZE)
            sess.run(train_step, feed_dict={x: batch_x, y: batch_y, keep_prob: 1.0})

        test_acc = sess.run(accuracy, feed_dict={x: mnist.test.images, y: mnist.test.labels, keep_prob: 1.0})
        train_acc = sess.run(accuracy, feed_dict={x: mnist.train.images, y: mnist.train.labels, keep_prob: 1.0})
        print("Iter:" + str(each)+" test accuracy:"+str(test_acc)+" train accuracy:"+str(train_acc))


